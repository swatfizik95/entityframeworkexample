﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EntityFrameworkExample.ConsoleApp.Migrations
{
    public partial class StateUser_Alter_Deleting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "StateUser",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[] { (byte)3, "Удаляется", "Deleting" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "StateUser",
                keyColumn: "Id",
                keyValue: (byte)3);
        }
    }
}
