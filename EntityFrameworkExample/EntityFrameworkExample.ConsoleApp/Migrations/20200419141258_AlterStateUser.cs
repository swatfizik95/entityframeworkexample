﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EntityFrameworkExample.ConsoleApp.Migrations
{
    public partial class AlterStateUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte>(
                name: "StateUserId",
                table: "Users",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.CreateTable(
                name: "StateUser",
                columns: table => new
                {
                    Id = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StateUser", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "StateUser",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[] { (byte)0, "", "NotActivated" });

            migrationBuilder.InsertData(
                table: "StateUser",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[] { (byte)1, "", "Active" });

            migrationBuilder.InsertData(
                table: "StateUser",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[] { (byte)2, "", "Deleted" });

            migrationBuilder.CreateIndex(
                name: "IX_Users_StateUserId",
                table: "Users",
                column: "StateUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_StateUser_StateUserId",
                table: "Users",
                column: "StateUserId",
                principalTable: "StateUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_StateUser_StateUserId",
                table: "Users");

            migrationBuilder.DropTable(
                name: "StateUser");

            migrationBuilder.DropIndex(
                name: "IX_Users_StateUserId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "StateUserId",
                table: "Users");
        }
    }
}
