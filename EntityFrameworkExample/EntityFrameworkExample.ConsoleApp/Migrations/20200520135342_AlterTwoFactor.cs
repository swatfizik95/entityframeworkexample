﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EntityFrameworkExample.ConsoleApp.Migrations
{
    public partial class AlterTwoFactor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TypeTwoFactorId",
                table: "Users",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "TypeTwoFactors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    ApiUrl = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TypeTwoFactors", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "TypeTwoFactors",
                columns: new[] { "Id", "ApiUrl", "Description", "Name" },
                values: new object[] { 0, null, "Смс", "Sms" });

            migrationBuilder.InsertData(
                table: "TypeTwoFactors",
                columns: new[] { "Id", "ApiUrl", "Description", "Name" },
                values: new object[] { 1, null, "Приложение", "Application" });

            migrationBuilder.CreateIndex(
                name: "IX_Users_TypeTwoFactorId",
                table: "Users",
                column: "TypeTwoFactorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_TypeTwoFactors_TypeTwoFactorId",
                table: "Users",
                column: "TypeTwoFactorId",
                principalTable: "TypeTwoFactors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_TypeTwoFactors_TypeTwoFactorId",
                table: "Users");

            migrationBuilder.DropTable(
                name: "TypeTwoFactors");

            migrationBuilder.DropIndex(
                name: "IX_Users_TypeTwoFactorId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "TypeTwoFactorId",
                table: "Users");
        }
    }
}
