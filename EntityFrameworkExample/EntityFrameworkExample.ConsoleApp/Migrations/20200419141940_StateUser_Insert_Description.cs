﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EntityFrameworkExample.ConsoleApp.Migrations
{
    public partial class StateUser_Insert_Description : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "StateUser",
                keyColumn: "Id",
                keyValue: (byte)0,
                column: "Description",
                value: "Неактивный");

            migrationBuilder.UpdateData(
                table: "StateUser",
                keyColumn: "Id",
                keyValue: (byte)1,
                column: "Description",
                value: "Аактивный");

            migrationBuilder.UpdateData(
                table: "StateUser",
                keyColumn: "Id",
                keyValue: (byte)2,
                column: "Description",
                value: "Удален");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "StateUser",
                keyColumn: "Id",
                keyValue: (byte)0,
                column: "Description",
                value: "");

            migrationBuilder.UpdateData(
                table: "StateUser",
                keyColumn: "Id",
                keyValue: (byte)1,
                column: "Description",
                value: "");

            migrationBuilder.UpdateData(
                table: "StateUser",
                keyColumn: "Id",
                keyValue: (byte)2,
                column: "Description",
                value: "");
        }
    }
}
