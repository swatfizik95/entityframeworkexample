﻿using System.Linq;
using EntityFrameworkExample.ConsoleApp.Data;
using EntityFrameworkExample.ConsoleApp.Entities;
using EntityFrameworkExample.ConsoleApp.Entities.Enums;

namespace EntityFrameworkExample.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //var user = new User()
            //{
            //    Name = "Name1"
            //};

            //using (var context = ExampleFactory.Instance.CreateExampleContext())
            //{
            //    user.StateUserId = StateUserEnum.Active;
            //    context.Users.Add(user);
            //    context.SaveChanges();
            //}

            using (var context = ExampleFactory.Instance.CreateExampleContext())
            {
                var user = context.Users.FirstOrDefault();
                user.StateUserId = StateUserEnum.Active;
                context.SaveChanges();
                user.TypeTwoFactorId = TypeTwoFactorEnum.Application;
                context.SaveChanges();
                user?.EnableTwoFactor(TypeTwoFactorEnum.Sms);
                context.SaveChanges();
            }
        }
    }
}
