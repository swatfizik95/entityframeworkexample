﻿using System;
using System.Linq;
using EntityFrameworkExample.ConsoleApp.Entities;
using EntityFrameworkExample.ConsoleApp.Entities.Abstracts;
using EntityFrameworkExample.ConsoleApp.Entities.Enums;
using EntityFrameworkExample.ConsoleApp.Entities.States;
using EntityFrameworkExample.ConsoleApp.Infrastructure.Extensions;
using Microsoft.EntityFrameworkCore;

namespace EntityFrameworkExample.ConsoleApp.Data
{
    public sealed class ExampleContext : DbContext
    {
        #region ctors

        public ExampleContext() : base(new DbContextOptionsBuilder<ExampleContext>()
            .UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=ExampleContext;Trusted_Connection=True;").Options)
        {
        }

        public ExampleContext(DbContextOptions<ExampleContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        #endregion

        #region entities

        public DbSet<User> Users { get; set; }
        public DbSet<StateUser> StateUser { get; set; }
        public DbSet<TypeTwoFactor> TypeTwoFactors { get; set; }

        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.SeedEnum<StateUser, StateUserEnum>();
            modelBuilder.SeedEnum<TypeTwoFactor, TypeTwoFactorEnum>();
        }

        public override int SaveChanges()
        {
            foreach (var entityEntry in ChangeTracker.Entries<BaseEntity>().Where(e => e.State == EntityState.Modified)) entityEntry.Entity.DateUpdate = DateTime.Now;

            return base.SaveChanges();
        }
    }
}