﻿using EntityFrameworkExample.ConsoleApp.Data.Abstracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace EntityFrameworkExample.ConsoleApp.Data
{
    public class ExampleFactory : IDbContextFactory
    {
        private ExampleFactory()
        {
        }

        public static ExampleFactory Instance => new ExampleFactory();
        
        public ExampleContext CreateExampleContext()
        {
            var builder = new ConfigurationBuilder();
            // установка пути к текущему каталогу
            builder.SetBasePath(Directory.GetCurrentDirectory());
            // получаем конфигурацию из файла appsettings.json
            builder.AddJsonFile("appsettings.json");
            // создаем конфигурацию
            var config = builder.Build();
            // получаем строку подключения
            string connectionString = config.GetConnectionString("DefaultConnection");

            var optionsBuilder = new DbContextOptionsBuilder<ExampleContext>();
            var options = optionsBuilder
                .UseSqlServer(connectionString)
                .Options;
            
            return new ExampleContext(options);
        }
    }
}