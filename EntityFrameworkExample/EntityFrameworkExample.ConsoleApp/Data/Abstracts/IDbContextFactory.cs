﻿using Microsoft.EntityFrameworkCore;

namespace EntityFrameworkExample.ConsoleApp.Data.Abstracts
{
    public interface IDbContextFactory
    {
        ExampleContext CreateExampleContext();
    }
}