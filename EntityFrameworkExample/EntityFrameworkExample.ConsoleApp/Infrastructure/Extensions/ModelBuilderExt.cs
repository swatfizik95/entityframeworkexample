﻿using EntityFrameworkExample.ConsoleApp.Entities.Abstracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace EntityFrameworkExample.ConsoleApp.Infrastructure.Extensions
{
    public static class ModelBuilderExt
    {
        public static void SeedEnum<TEnumTable, TEnum>(this ModelBuilder modelBuilder)
            where TEnumTable : EnumEntity<TEnum> where TEnum : struct
        {
            modelBuilder
                .Entity<TEnumTable>().HasData(
                    Enum.GetValues(typeof(TEnum))
                        .Cast<TEnum>()
                        .Select(su => new EnumEntity<TEnum>
                        {
                            Id = su,
                            Name = su.ToString(),
                            Description = su.GetDescription()
                        })
                );
        }
    }
}