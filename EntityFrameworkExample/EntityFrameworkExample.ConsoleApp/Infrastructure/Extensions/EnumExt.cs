﻿using System.ComponentModel;
using System.Linq;

namespace EntityFrameworkExample.ConsoleApp.Infrastructure.Extensions
{
    public static class EnumExt
    {
        public static string GetDescription<TEnum>(this TEnum item)
            => item.GetType()
                   .GetField(item.ToString())
                   .GetCustomAttributes(typeof(DescriptionAttribute), false)
                   .Cast<DescriptionAttribute>()
                   .FirstOrDefault()?.Description ?? string.Empty;
    }
}