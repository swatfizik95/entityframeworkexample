﻿using System.ComponentModel;

namespace EntityFrameworkExample.ConsoleApp.Entities.Enums
{
    public enum TypeTwoFactorEnum
    {
        [Description("Смс")]
        Sms,
        [Description("Приложение")]
        Application
    }
}