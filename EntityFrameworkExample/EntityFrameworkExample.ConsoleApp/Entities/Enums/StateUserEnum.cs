﻿using System.ComponentModel;

namespace EntityFrameworkExample.ConsoleApp.Entities.Enums
{
    public enum StateUserEnum : byte
    {
        [Description("Неактивный")]
        NotActivated,
        [Description("Аактивный")]
        Active,
        [Description("Удален")]
        Deleted,
        [Description("Удаляется")]
        Deleting
    }
}