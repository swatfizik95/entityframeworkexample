﻿using EntityFrameworkExample.ConsoleApp.Entities.Abstracts;
using EntityFrameworkExample.ConsoleApp.Entities.Enums;
using EntityFrameworkExample.ConsoleApp.Entities.States;

namespace EntityFrameworkExample.ConsoleApp.Entities
{
    public class User : BaseEntity
    {
        public string Name { get; set; }
        public StateUserEnum StateUserId { get; set; }
        public StateUser StateUser { get; set; }
        public TypeTwoFactorEnum? TypeTwoFactorId { get; set; }
        public TypeTwoFactor TypeTwoFactor { get; set; }

        #region methods

        public void EnableTwoFactor(TypeTwoFactorEnum typeTwoFactor) => TypeTwoFactorId = typeTwoFactor;

        #endregion
    }
}