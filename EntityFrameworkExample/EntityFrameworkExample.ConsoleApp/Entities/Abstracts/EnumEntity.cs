﻿namespace EntityFrameworkExample.ConsoleApp.Entities.Abstracts
{
    public class EnumEntity<TEnum> where TEnum: struct
    {
        public TEnum Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}