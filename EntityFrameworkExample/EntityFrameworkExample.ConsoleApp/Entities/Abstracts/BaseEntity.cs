﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EntityFrameworkExample.ConsoleApp.Entities.Abstracts
{
    public abstract class BaseEntity
    {
        [Key]
        public int Id { get; set; }
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime DateCreate { get; set; } = DateTime.Now;
        
        public DateTime? DateUpdate { get; set; } = null;
    }
}