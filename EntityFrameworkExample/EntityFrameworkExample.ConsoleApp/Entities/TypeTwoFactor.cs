﻿using EntityFrameworkExample.ConsoleApp.Entities.Abstracts;
using EntityFrameworkExample.ConsoleApp.Entities.Enums;
using System.ComponentModel.DataAnnotations;

namespace EntityFrameworkExample.ConsoleApp.Entities
{
    public class TypeTwoFactor : EnumEntity<TypeTwoFactorEnum>
    {
        [MaxLength(100)]
        public string ApiUrl { get; set; }
    }
}